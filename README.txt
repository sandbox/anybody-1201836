README.txt
----------
Fast and easy Tasklists / TODO Lists!

Converts a textual tasks list with a set state symbol (Done / To do) 
to a beautiful checkbox lists display in output.

For example:
# Wash the dishes
- Take a shower
-{ Play with the kids outside, for example
Football
or Soccer}

Will become a nicely styled list with a checked first icon, an unchecked second 
and an unchecked multiline third icon.

Inspired by:
http://drupal.org/project/todo_filter
which has:
- no multiline support
- no possibility for configuration
- less intuitive and quick syntax
- but therefore theoretically AJAX functionality, which isn't working currently 
(and is not planned for well_done_filter!!)
All in all well_done_filter is an alternative with a different purpose and
 not a replacement for all use cases perhaps.

DEPENDENCIES
------------
- none -

INSTALLATION
------------
1. Download and enable this module
2. Edit your input formats ("admin/settings/filters/list") and enable the "Well done filter" for the formats you want to use it for.
3. You may customize your settings at "admin/settings/well_done_filter"

AUTHOR/MAINTAINER
-----------------
- Julian Pustkuchen (http://Julian.Pustkuchen.com)
- Development sponsored by:
    webks: websolutions kept simple (http://www.webks.de)
      and
    DROWL: Drupalbasierte Lösungen aus Ostwestfalen-Lippe (http://www.drowl.de)