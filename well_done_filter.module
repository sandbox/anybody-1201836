<?php

/**
 * @file
 *
 * Implements the well done filter core functionality and configuration page.
 */

// == Core hooks ===============================================================


function well_done_filter_help($path = 'admin/help#well_done_filter', $arg) {
  switch ($path) {
    case 'admin/help#well_done_filter':
      return t('<p>Fast and easy Tasklists / TODO Lists! Display tasks with a corresponding symbol to show their (un)checked state from syntax.</p>');
      break;
    case 'admin/settings/well_done_filter':
      return t('Feel free to configure your own syntax and output for the well done filter. Notice: Don\'t forget to set your filter @here.', array('!here' => l(t('here'), 'admin/settings/filters/list')));
  }
}

/**
 * Implementation of hook_menu().
 *
 * @return array The menu items.
 */
function well_done_filter_menu() {
  $items = array();
  $items['admin/settings/well_done_filter'] = array(
    'title' => 'Well done filter',
    'description' => 'Display tasks with a corresponding symbol to show their (un)checked state from syntax.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array(
      'well_done_filter_admin_settings'
    ),
    'access callback' => 'user_access',
    'access arguments' => array(
      'administer site configuration'
    ),
    'type' => MENU_NORMAL_ITEM
  );
  return $items;
}

/**
 * Administration form settings.
 * @return array The form settings.
 */
function well_done_filter_admin_settings() {
  $form = array();

  $form['symbols'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement syntax'),
    '#description' => t('Configure the symbols / characters used to render the tasks in your texts.<br />Due to performance issues currently only characters and not string combinations are supported.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE
  );

  $form['symbols']['well_done_filter_symbols_checked'] = array(
    '#type' => 'textfield',
    '#title' => t('Checked symbol'),
    '#default_value' => check_plain(variable_get('well_done_filter_symbols_checked', '#')),
    '#description' => t('The symbol (as first char of the line) that represents the "done" / "checked" state. <em>Default: "#"</em>'),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE
  );

  $form['symbols']['well_done_filter_symbols_unchecked'] = array(
    '#type' => 'textfield',
    '#title' => t('Unchecked symbol'),
    '#default_value' => check_plain(variable_get('well_done_filter_symbols_unchecked', '-')),
    '#description' => t('The symbol (as first char of the line) that represents the "to do" / "unchecked" state. <em>Default: "-"</em>'),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE,
  );

  $form['symbols']['well_done_filter_symbols_from'] = array(
    '#type' => 'textfield',
    '#title' => t('From symbol'),
    '#default_value' => check_plain(variable_get('well_done_filter_symbols_from', '{')),
    '#description' => t('The symbol (as second char of the line) that is used to indicate a multiline task. <em>Default: "{"</em>'),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE
  );

  $form['symbols']['well_done_filter_symbols_to'] = array(
    '#type' => 'textfield',
    '#title' => t('To symbol'),
    '#default_value' => check_plain(variable_get('well_done_filter_symbols_to', '}')),
    '#description' => t('The symbol that ends a multiline task. <em>Default: "}"</em>'),
    '#size' => 1,
    '#maxlength' => 1,
    '#required' => TRUE
  );

  $form['code'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement code'),
    '#description' => t('Advanced users can change the output of the replacements here.<br />So you may for example add own classes, use icons or do whatever you like!'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );

  $options = array(
    'span' => t('Inline'),
    'div' => t('Block')
  );

  $form['code']['well_done_filter_code_format'] = array(
    '#type' => 'select',
    '#title' => t('Format'),
    '#options' => $options,
    '#default_value' => check_plain(variable_get('well_done_filter_code_format', 'span')),
    '#description' => t('Select the output format to be inline ("%span") or block ("%div"). <em>Default: "Inline"</em>', array('%span' => '<span>', '%div' => '<div>')),
    '#required' => TRUE
  );

  $form['code']['well_done_filter_checked_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Checked icon'),
    '#default_value' => variable_get('well_done_filter_checked_icon',
    '<input type="checkbox" disabled="disabled" checked="checked" />'),
    '#description' => t('The HTML Code used to represent the checked state.<br /><em>Default: "%code"</em>', array('%code' => '<input type="checkbox" disabled="disabled" checked="checked" />)')),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE
  );

  $form['code']['well_done_filter_unchecked_icon'] = array(
    '#type' => 'textfield',
    '#title' => t('Unchecked icon'),
    '#default_value' => variable_get('well_done_filter_unchecked_icon',
    '<input type="checkbox" disabled="disabled" />'),
    '#description' => t('The HTML Code used to represent the unchecked state.<br /><em>Default: "%code"</em>', array('%code' => '<input type="checkbox" disabled="disabled" /> ')),
    '#size' => 120,
    '#maxlength' => 255,
    '#required' => TRUE
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_filter().
 */
function well_done_filter_filter($op, $delta = 0, $format = -1, $text = '') {
  switch ($op) {
    case 'list':
      return array(
        t('Well done style')
      );
    case 'description':
      return t('Display tasks with a corresponding symbol to show their (un)checked state from syntax.');
    case 'settings':
      return well_done_filter_admin_settings($format);
    case 'process':
      return _well_done_filter_process($text);
    default:
      return $text;
  }
}

function well_done_filter_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  switch ($op) {
    case 'view':
      drupal_add_css(drupal_get_path('module', 'well_done_filter') . '/well_done_filter.css', 'module');
      $node->content['body']['#value'] = str_replace('class="well_done-check" rel="', 'disabled class="well_done-check" rel="' . $node->nid . '-', $node->content['body']['#value']);
      break;
  }
}

/**
 * Implementation of hook_filter_tips().
 */
function well_done_filter_filter_tips($delta = 0, $format = -1, $long) {
  return t('Quick tips:') . _well_done_filter_help_block();
}

// == Internal functions =======================================================


/**
 * Provides content for the well_done filter help block.
 */
function _well_done_filter_help_block() {
  return t('<ul>
      <li>"%well_done_filter_symbols_unchecked" as first char:  It will show checkbox that isn\'t checked</li>
      <li>"%well_done_filter_symbols_checked" as first char: It will show checkbox that is checked</li>
      <li>If the char is directly followed by an opening sign ("%well_done_filter_symbols_from"), then multiline tasks are possible. But don\'t forget closing.</li>
  	</ul>', array(
    '%well_done_filter_symbols_unchecked' => variable_get('well_done_filter_symbols_unchecked', '-'), '%well_done_filter_symbols_checked' => variable_get('well_done_filter_symbols_checked', '#'), '%well_done_filter_symbols_from' => variable_get('well_done_filter_symbols_from', '{')
  ));
}

/**
 * Filter process callback.
 */
function _well_done_filter_process($text) {
  if (! empty($text)) {
    // Get variables
    $checked_symbol = check_plain(variable_get('well_done_filter_symbols_checked', '#'));
    $unchecked_symbol = check_plain(variable_get('well_done_filter_symbols_unchecked', '-'));
    $range_from_symbol = check_plain(variable_get('well_done_filter_symbols_from', '{'));
    $range_to_symbol = check_plain(variable_get('well_done_filter_symbols_to', '}'));
    $code_format = check_plain(variable_get('well_done_filter_code_format', 'span'));
    $close_replacement = '</' . $code_format . '>';
    $replacements = array(
      $checked_symbol => '<' . $code_format . ' class="well-done-item checked">' . variable_get('well_done_filter_checked_icon', '<input type="checkbox" disabled="disabled" checked="checked" /> '), $unchecked_symbol => '<' . $code_format . ' class="well-done-item unchecked">' . variable_get('well_done_filter_unchecked_icon', '<input type="checkbox" disabled="disabled" /> ')
    );
    $is_open = FALSE;
    $replacement_keys = array_keys($replacements);
    $text_array = explode("\n", trim($text));
    foreach ($text_array as $key => $line) {
      $line_raw = trim(strip_tags($line));
      // Can we close the open area in this line?
      if ($is_open) {
        $line = _well_done_filter_replace_first_occurence($range_to_symbol, $close_replacement, $line, $found);
        if ($found) {
          $is_open = FALSE;
        }
      }
      // Process line
      $first_char = $line_raw[0];
      if (in_array($first_char, $replacement_keys)) {
        foreach ($replacements as $symbol => $replacement) {
          if ($first_char == $symbol) {
            if ($is_open) {
              // Close anyway before opening the next one.
              $line = $close_replacement . $line;
              $is_open = FALSE;
            }
            $line = _well_done_filter_replace_first_occurence($symbol, $replacement, $line);
            break;
          }
        }
        // Check if a range is used
        $second_char = $line_raw[1];
        if ($second_char == $range_from_symbol) {
          $line = _well_done_filter_replace_first_occurence($range_from_symbol, '', $line);
          $is_open = TRUE;
          // Can we close the open area in the same line already?
          $line = _well_done_filter_replace_first_occurence($range_to_symbol, $close_replacement, $line, $found);
          if ($found) {
            $is_open = FALSE;
          }
         // No closing yet
        }
        else {
          // Close after line
          $line .= $close_replacement;
        }
      }
      $text_array[$key] = $line;
    }
    if ($is_open) {
      // Close if last line is still open
      $text_array[$key] .= $close_replacement;
    }
    $text = implode("\n", $text_array);
  }
  return $text;
}

/**
 * Helper function to replace a first occurence of a text.
 */
function _well_done_filter_replace_first_occurence($search, $replace, $context, &$found = FALSE) {
  $result = preg_replace('/' . preg_quote($search) . '/', $replace, $context, 1, $count);
  $found = ! empty($count);
  return $result;
}